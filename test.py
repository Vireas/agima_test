#!/usr/bin/env python
# -*- coding: utf-8 -*-
import collections
import json
import unittest

from collector import word_assembly, row_assembly, recursion


class TestMethods(unittest.TestCase):

    def test_word_assembly(self):
        result = word_assembly(line='Hello', key='h1')
        self.assertEqual(result, '<h1>Hello</h1>')

    def test_row_assembly(self):
        result = row_assembly({"p": "Example 1"})
        self.assertEqual(result, '<p>Example 1</p>')

    def test_recursion(self):
        json_data = """{
            "span": "Title #1",
            "content": [
                {
                    "p": "Example 1",
                    "header": "header 1"
                }
            ]
        }"""
        data = json.loads(json_data, object_pairs_hook=collections.OrderedDict)
        result = recursion(json_data=data, type_is_list=False, text='')
        self.assertEqual(result, '<span>Title #1</span><content><ul><li><p>Example 1</p><header>header 1</header></li></ul></content>')


if __name__ == '__main__':
    unittest.main()
