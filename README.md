**Тест-кейсы**

*Работа с файлом:*

|Действие|Ожидаемый результат|
|--------|:------------------:|
|Открыть пустой json-файл|Пустой файл на выходе| 
|Открыть несуществующий файл|Оповещение, не удалось открыть файл|



*Проверка функций:*

|Функция|Входящие значения|Ожидаемый результат|
|--------|--------|:------------------:|
|word_assembly(line, key)|line='Hello', key='h1'|<h1>Hello</h1> 
|row_assembly(data_dict)|{"p": "Example1"}|<p>Example1</p>
|row_assembly(data_dict)|[{"p": "Example1"}]|<ul><li><p>Example1</p></li></ul>
|recursion(json_data, type_is_list, text)|{"span": "Title#1","content": [{"p": "Example1","header": "header1"}]}|<span>Title#1</span><content><ul><li><p>Example1</p><header>header1</header></li></ul></content>

