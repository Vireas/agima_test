# -*- coding: utf-8 -*-
import re
import cgi
import json
import os.path
import collections


def find_style(line):
    """
    разбиение строки, для поиска тега и стилей.  сборка строки в формате для html
    :param line: строка, которая содержит тег и его стили
    :return: возвращает тег и стили для него
    """
    tag = ''.join(re.findall(r'(^[a-zA-Z0-9]+)', line))
    get_class = re.findall(r'\.([-a-zA-Z0-9_]+)', line)
    get_id = re.findall(r'\#([-a-zA-Z0-9_]+)', line)
    attrs = ' '
    if get_id:
        attrs += 'id="' + ' '.join(get_id) + '" '
    if get_class:
        attrs += 'class="' + ' '.join(get_class) + '" '
    return tag, attrs.rstrip()


def word_assembly(line, key):
    """
    :param line: content
    :param key: tag
    :return: возвращает собранную строку из tag и content 
    """
    tag, attrs = find_style(key)
    escape_line = cgi.escape(line)
    text = '<{tag}{attrs}>{line}</{tag}>'.format(tag=tag, attrs=attrs, line=escape_line)
    return text


def row_assembly(data_dict):
    """
    Функция, которая рекурсивно бегает по словарю, если есть вложенность 
    :param data_dict: словарь
    :return: возвращает собранный content для html
    """
    text = ''
    for key in data_dict.keys():
        type_is_list = isinstance(data_dict[key], list)
        if type_is_list:
            text += '<{tag}><ul>{line}</ul></{tag}>'.format(tag=key, line=recursion(data_dict[key], type_is_list, ''))
        else:
            text += word_assembly(data_dict[key], key)
    return text


def recursion(json_data, type_is_list, text):
    """
    :param json_data: Словарь/список с данными
    :param type_is_list: на вход функции пришел словарь или список? (True/False)
    :param text: переменная, для сборки content
    :return: ны выходе из рекурсии выдаеся переменная, которая содержит собранный content
    """
    if type_is_list:
        for data in json_data:
            text += '<li>' + row_assembly(data) + '</li>'
    else:
        text += row_assembly(json_data)
    return text


def task():
    file_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'source.json'))
    if not os.path.exists(file_path):
        print (u'Не удалось открыть файл')
    json_file = open('source.json').read()
    text = ''
    if json_file:
        json_data = json.loads(json_file, object_pairs_hook=collections.OrderedDict)
        type_is_list = isinstance(json_data, list)
        if type_is_list:
            text = '<ul>' + recursion(json_data, type_is_list, '') + '</ul>'
        else:
            text = recursion(json_data, type_is_list, '')
    return text